# Blacknet Web Browser Extension

The aim of this project is to provide an in-browser wallet for Blacknet.
This would make it possible to present a Blacknet blockchain application (BApp) as a web site.
A transaction content must be by the user before signing.
